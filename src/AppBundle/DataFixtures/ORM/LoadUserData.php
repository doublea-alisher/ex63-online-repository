<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData extends Fixture
{
    public function load(ObjectManager $manager){
        $user = new User();
        $user->setEmail('12345@mail.ru')
            ->setPassword(password_hash('123qwe',PASSWORD_BCRYPT))
            ->setUsername('Rus')
            ->setRoles(['ROLE_USER'])
            ->setEnabled(true);
        $manager->persist($user);

        $user = new User();
        $user->setEmail('zxc@mail.ru')
            ->setPassword(password_hash('123qwe',PASSWORD_BCRYPT))
            ->setUsername('Fedor')
            ->setRoles(['ROLE_USER'])
            ->setEnabled(true);
        $manager->persist($user);

        $manager->flush();
    }
}