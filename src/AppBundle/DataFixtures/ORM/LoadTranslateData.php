<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Note;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadTranslateData extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $note = new Note();
        $note->translate('ru')->setText('Переведите пожалуйста');
        $note->translate('en')->setText('Translate please');
        $note->translate('kg')->setText('Жылуу сураныч');
        $note->translate('cn')->setText('請翻譯');
        $note->translate('fr')->setText('Traduire s\'il vous plaît');
        $note->translate('ua')->setText('Переведіть будь ласка');
        $manager->persist($note);
        $note->mergeNewTranslations();

        $note = new Note();
        $note->translate('ru')->setText('У меня новая машина');
        $note->translate('en')->setText('I have a new car');
        $note->translate('kg')->setText('Мен жаңы унаа бар');
        $note->translate('cn')->setText('我有一輛新車');
        $note->translate('fr')->setText('J\'ai une nouvelle voiture');
        $manager->persist($note);
        $note->mergeNewTranslations();

        $note = new Note();
        $note->translate('ru')->setText('Я на выходных лечу в Лондон');
        $note->translate('en')->setText('I\'m flying to London on weekends');
        $note->translate('kg')->setText('Мен Лондонго учуп дем алыш күндөрү жатам');
        $note->translate('cn')->setText('我周末飛往倫敦');
        $manager->persist($note);
        $note->mergeNewTranslations();

        $note = new Note();
        $note->translate('ru')->setText('Этот сайт лучший');
        $note->translate('en')->setText('This site is the best');
        $manager->persist($note);
        $note->mergeNewTranslations();

        $note = new Note();
        $note->translate('ru')->setText('Кто переведет тот дурак');
        $manager->persist($note);
        $note->mergeNewTranslations();

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            LoadUserData::class,
        );
    }
}