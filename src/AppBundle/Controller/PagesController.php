<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PagesController
 * @package AppBundle\Controller
 */
class PagesController extends Controller
{
    /**
     * @Route("/")
     * @return Response
     */
    public function indexAction()
    {
        $notes = $this->getDoctrine()->getRepository('AppBundle:Note')->findAll();
        return $this->render('@App/Pages/index.html.twig', array(
            'notes' => $notes,
        ));
    }

    /**
     * @Route("/{id}/view/")
     * @param $id int
     * @return Response
     */
    public function viewAction($id)
    {
        $note = $this->getDoctrine()->getRepository('AppBundle:Note')->find($id);
        return $this->render("@App/Pages/view.html.twig", [
            'note' => $note,
        ]);
    }

    /**
     * @Route("/{_locale}/lang", requirements = {"_locale" : "en|ru"})
     */
    public function changeLocale(){
        return new Response();
    }
}
