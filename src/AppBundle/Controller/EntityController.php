<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Note;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class EntityController extends Controller
{
    /**
     * @Route("/new")
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        if ($text = $request->get('text')) {
            $note = new Note();
            $note->translate('ru')->setText($text);
            $em = $this->getDoctrine()->getManager();
            $em->persist($note);
            $note->mergeNewTranslations();
            $em->flush();
        };
        return $this->redirectToRoute('app_pages_index');
    }

    /**
     * @Route("/{id}/edit")
     * @param Request $request
     * @param $id int
     * @return Response
     */
    public function editTranslate(Request $request, $id)
    {
        $data = $request->get('lang');

        $note = $this->getDoctrine()->getRepository('AppBundle:Note')->find($id);

        foreach ($data as $locale => $text) {
            if (trim($text)) {
                $note->translate("{$locale}",false)->setText($text);
            }
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($note);
        $note->mergeNewTranslations();
        $em->flush();

        return $this->redirectToRoute('app_pages_view', [
            'id' => $id
        ]);
    }
}